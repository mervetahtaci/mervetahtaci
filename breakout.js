
 (function() {
  drawTable();

  var level = [
    '**************',
    '**************',
    '**************',
    '**************'
  ];

  var gameLoop;
  var gameSpeed =   10;
  var ballMovementSpeed = 3;

  var bricks = [];
  var bricksMargin = 1;
  var bricksWidth = 0;
  var bricksHeight = 18;
  var rightPressed = false;//bunu koyduklarımı ben yazmıştım ona göre incelersin xd
  var leftPressed = false;//
  var scorePoint=0;//
  var scoreText;//
  var lives=3;//
  var livesText;//
  var playing=false;

  var ball = {
    width: 6,
    height: 6,
    left: 0,
    top: 0,
    speedLeft: 0,
    speedTop: 0
  };

  var paddle = {
    width: 100,
    height: 6,
    left: (document.getElementById('breakout').offsetWidth / 2) - 30,
    top: document.getElementById('breakout').offsetHeight - 40
  };

  function startGame() {
    resetBall();
    buildLevel();
    createBricks();
    updateObjects();
  }

  function drawTable() {
    document.body.style.background = '#0E5CAD';
    document.body.style.font = '18px Orbitron';
    document.body.style.color = '#FFF';

    var breakout = document.createElement('div');
    var paddle = document.createElement('div');
    var ball = document.createElement('div');
    var liveBar = document.createElement('div');
    var scoreBar = document.createElement('div');
    livesText=liveBar;
    scoreText=scoreBar;
    breakout.id = 'breakout';
    breakout.style.width = '800px';
    breakout.style.height = '600px';
    breakout.style.position = 'fixed';
    breakout.style.left = '50%';
    breakout.style.top = '50%';
    breakout.style.transform = 'translate(-50%, -50%)';
    breakout.style.background = '#000000';

    paddle.id = 'paddle';
    paddle.style.background = '#E80505';
    paddle.style.position = 'absolute';
    paddle.style.boxShadow = '0 15px 6px -2px rgba(0,0,0,.6)';

    ball.className = 'ball';
    ball.style.position = 'absolute';
    ball.style.background = '#FFF';
    ball.style.boxShadow = '0 15px 6px -1px rgba(0,0,0,.6)';
    ball.style.borderRadius = '50%';
    ball.style.zIndex = '9';

    liveBar.className = 'liveBar';
    liveBar.style.position = 'absolute';
    liveBar.style.boxShadow = '0 15px 6px -1px rgba(0,0,0,.6)';
    liveBar.style.zIndex = '9';

    scorePoint=0;
    scoreBar.className = 'scoreBar';
    scoreBar.style.position = 'absolute';
    scoreBar.style.top='20px';
    scoreBar.style.boxShadow = '0 15px 6px -1px rgba(0,0,0,.6)';
    scoreBar.style.zIndex = '9';
    scoreBar.innerHTML="Score:"+scorePoint;

    breakout.appendChild(paddle);
    breakout.appendChild(ball);
    breakout.appendChild(liveBar);
    breakout.appendChild(scoreBar);
    document.body.appendChild(breakout);
  }

  function removeElement(element) {
    if (element && element.parentNode) {
      element.parentNode.removeChild(element);
    }
  }

  function buildLevel() {
    var arena = document.getElementById('breakout');

    bricks = [];

    for (var row = 0; row < level.length; row++) {
      for (var column = 0; column <= level[row].length; column++) {

        if (!level[row][column] || level[row][column] === ' ') {
          continue;
        }

        bricksWidth = (arena.offsetWidth - bricksMargin * 2) / level[row].length;

        bricks.push({
          left: bricksMargin * 2 + (bricksWidth * column),
          top: bricksHeight * row + 60,
          width: bricksWidth - bricksMargin * 2,
          height: bricksHeight - bricksMargin * 2,
          removed: false
        });
      }
    }
  }

  function removeBricks() {

    document.querySelectorAll('.brick').forEach(function(brick) {
      removeElement(brick);
    });
    bricks.forEach(b=>b.removed=false);
  }

  function createBricks() {
    removeBricks();

    var arena = document.getElementById('breakout');

    bricks.forEach(function(brick, index) {
      var element = document.createElement('div');

      element.id = 'brick-' + index;
      element.className = 'brick';
      element.style.left = brick.left + 'px';
      element.style.top = brick.top + 'px';
      element.style.width = brick.width + 'px';
      element.style.height = brick.height + 'px';
      element.style.background = '#FFFFFF';
      element.style.position = 'absolute';
      element.style.boxShadow = '0 15px 20px 0px rgba(0,0,0,.4)';

      arena.appendChild(element)
    });
  }

  function updateObjects() {
    document.getElementById('paddle').style.width = paddle.width + 'px';
    document.getElementById('paddle').style.height = paddle.height + 'px';
    document.getElementById('paddle').style.left = paddle.left + 'px';
    document.getElementById('paddle').style.top = paddle.top + 'px';

    document.querySelector('.ball').style.width = ball.width + 'px';
    document.querySelector('.ball').style.height = ball.height + 'px';
    document.querySelector('.ball').style.left = ball.left + 'px';
    document.querySelector('.ball').style.top = ball.top + 'px';
  }

  function resetBall() {
    var arena = document.getElementById('breakout');

    ball.left = (arena.offsetWidth / 2) - (ball.width / 2);
    ball.top = (arena.offsetHeight / 1.6) - (ball.height / 2);
    ball.speedLeft = 0;
    ball.speedTop = ballMovementSpeed;

    document.querySelector('.ball').style.left = ball.left + 'px';
    document.querySelector('.ball').style.top = ball.top + 'px';
    lives--;

    if(lives<0){
      createBricks();
      lives = 3;
      scorePoint=0;
      scoreText.innerHTML="Score:"+scorePoint;

    }
    livesText.innerHTML="lives:"+ (lives);

  }

  function movePaddle(clientX) {
    var arena = document.getElementById('breakout');
    var arenaRect = arena.getBoundingClientRect();
    var arenaWidth = arena.offsetWidth;
    var mouseX = clientX - arenaRect.x;
    var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;

    if (mouseX <= halfOfPaddle) {
      mouseX = halfOfPaddle;
    }

    if (mouseX >= arenaWidth - halfOfPaddle) {
      mouseX = arenaWidth - halfOfPaddle;
    }

    paddle.left = mouseX - halfOfPaddle;

  }
  function movePaddleWithKeyboard() {
    var arena = document.getElementById('breakout');
    var arenaWidth = arena.offsetWidth;
    var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;//50
    if (paddle.left>0 && leftPressed) {
      paddle.left -=10
    }

    if (arenaWidth-2*halfOfPaddle> paddle.left && rightPressed) {
      paddle.left +=10
    }

  }

  function moveBall() {

    detectCollision();

    var arena = document.getElementById('breakout');

    ball.top += ball.speedTop;
    ball.left += ball.speedLeft;

    if (ball.left <= 0 || ball.left + ball.width >= arena.offsetWidth) {
      ball.speedLeft = -ball.speedLeft;
    }

    if (ball.top <= 0 || ball.top + ball.height >= arena.offsetHeight) {
      ball.speedTop = -ball.speedTop;
    }

    if (ball.top + ball.height >= arena.offsetHeight) {
      resetBall();
    }
  }

  function detectCollision() {
    if (ball.top + ball.height >= paddle.top &&
      ball.top + ball.height <= paddle.top + paddle.height &&
      ball.left >= paddle.left &&
      ball.left <= paddle.left + paddle.width
    ) {

      var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;//50
      var centerx = ball.left+ball.width/2;
      var collision = centerx - (paddle.left+halfOfPaddle);
      collision = collision/halfOfPaddle;
      var angle = collision * Math.PI/3;
      var speed = Math.sqrt(ball.speedTop**2+ball.speedLeft**2);
      ball.speedLeft = speed * Math.sin(angle);
      ball.speedTop = -speed * Math.cos(angle);
    }

    for (var i = 0; i < bricks.length; i++) {
      var brick = bricks[i];

      if (ball.top + ball.height >= brick.top &&
        ball.top <= brick.top + brick.height &&
        ball.left + ball.width >= brick.left &&
        ball.left <= brick.left + brick.width &&
        !brick.removed
      ) {
        document.getElementById("brick-"+i).remove();
        bricks[i].removed=true;
        ball.speedTop = -ball.speedTop;
        scorePoint+=10;
        scoreText.innerHTML="Score:"+scorePoint;

        break;
      }
    }
  }

  function setEvents() {
    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('keyup', keyUpHandler, false);

    function keyDownHandler(e) {
      if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = true;
      } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = true;
      }
      movePaddleWithKeyboard()
    }

   function keyUpHandler(e) {
      if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = false;
      } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = false;
      }
      movePaddleWithKeyboard()
    }

    document.addEventListener('mousemove', function(event) {
      movePaddle(event.clientX);
    });

  }

  function startGameLoop() {
    gameLoop = setInterval(function() {
      moveBall();
      updateObjects();
    }, gameSpeed);
  }

  setEvents();
  startGame();
  startGameLoop();
})();
